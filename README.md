PluJson version 0.03 2016-06-30
==================================
by David Lhoumaud  
craft at ckdevelop.org  
  
CMS sans base de donnée, il utilise le format de fichier JSON pour stocker les informations.  
  
SCREENSHOT
==========
![Capture](https://framagit.org/BeHuman/pluJson/raw/41311d7855b9de5b74711fcc98c8f5f9291d435b/screenshot/BO_001.png)
![Capture](https://framagit.org/BeHuman/pluJson/raw/41311d7855b9de5b74711fcc98c8f5f9291d435b/screenshot/BO_002.png)
![Capture](https://framagit.org/BeHuman/pluJson/raw/41311d7855b9de5b74711fcc98c8f5f9291d435b/screenshot/frontEnd.png)
  
  
ACCES
=====
# FrontOffice
http://localhost/index.php  
or  
http://127.0.0.1/index.php  
  
# BackOffice (admin):
http://localhost/admin.php  
or  
http://127.0.0.1/admin.php  
## Identification BackOffice
user : admin  
password : admin  

Options disponible
------------------
- Création d'articles  
- Gestion des commentaires par article avec capcha  
- Création de pages statiques  
- Gestion des catégories  
- Gestion du menu principal  
- Gestion page de contact avec capcha  
