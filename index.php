<?php
include('includes/php/init.php');
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include('includes/tpl/head.php'); ?>
    </head>
    <body onload="//loadBackground();">
        <!--<div id="wrapperBackground">
        <img src="includes/imgs/silL.png" style="float:right;">
        <img src="includes/imgs/silR.png" style="float:left;">
        </div>-->
        <?php include('includes/tpl/header.php'); ?>
        <div id="globalmain">
        <?php if ($SIDEBAR=='left') {include('includes/tpl/sidebar.php');} ?>
        <?php include('includes/tpl/content.php'); ?>
        <?php if ($SIDEBAR=='right') {include('includes/tpl/sidebar.php');} ?>
        </div>
        <?php include('includes/tpl/footer.php'); ?>
        
    </body>
</html>
