<style type="text/css">
@font-face {
    font-family: "gamecube";
    src: url('includes/fonts/GameCube.ttf');
}

@media all and (max-width: 999px) {
    body  {
        font-family: auto, Helvetica, Arial, sans-serif;
        background-color: #efefef;
        margin:0% 0% 0% 0%;
        color:#333;
    }
}
@media all and (max-width: 774px) {
    body #wrapperBackground {
        display:none;
    }
}
@media all and (min-width: 776px) {
    body #wrapperBackground {

        position:fixed;
        left:0px;
        top:0px;
        width:100%;
        height:100%;
        z-index:-1;
        
        background-color: #efefef;
        background-repeat:no-repeat;
        
        transition:opacity 2s;
    }
}
@media all and (min-width: 1000px)  {
    body  {
        font-family: auto, Helvetica, Arial, sans-serif;
        background-color: #efefef;
        background-image: url(includes/imgs/bugblue.png);
        background-position: left top;
        background-repeat: no-repeat;
        margin:0% 15% 0% 15%;
        color:#333;
    }
}
a {
color:#6DA406;
}
body>footer {
    opacity:1;
    position:relative;
    clear:both;
    margin-top:50px;
    padding:10px;
    text-align:center;
    background-color:#6DA406;
/*    background-image:linear-gradient(135deg, #000 70%, #000 70%,#fff 70%, #fff 70.5%,#edd400 70.5%, #edd400 90%, #fff 90.5%,#fff 91%, transparent 91%);*/
/*    background-repeat:no-repeat;*/
    color:#eee;
    box-shadow:0px -5px 5px #999;
}

#header {
    opacity:1;
    top:0px;
    background-color:#6DA406;
/*    background-image:linear-gradient(135deg, #000 25%, #000 25%,#fff 25%, #fff 25.5%,#edd400 25.5%, #edd400 90%);*/
/*    background-image:url(includes/imgs/slbb_team.jpg);*/
/*    background-position:-80px -120px;*/
/*    background-repeat:no-repeat;*/
    box-shadow:0px 5px 5px #999;
/*    height:100px;*/
}
#header #name_site{
    margin:0px 0px 20px 0px;
    padding:10px 0px 0px 0px;
    color:#333;
    
}

#header  #logo{
    float:right;
    height:170px;
    width:auto;

}


@media all and (min-width: 1093px) {
    #header #name_site {
        color:#333;
        text-shadow:2px 2px 5px #444;
        font-size:2em;
    }
    #header #sub_name_site {
        color:#444;
        font-size:1em;
    }
    #header #logo{
        float:right;
        height:170px;
        width:auto;
    }
}
@media all and (min-width: 999px) {
    #header #name_site {
        color:#333;
        text-shadow:2px 2px 5px #444;
        font-size:1.5em;
    }
    #header #sub_name_site {
        color:#444;
        font-size:0.75em;
    }
    #header #logo{
        float:right;
        height:150px;
        width:auto;
    }
}
@media all and (min-width: 775px) {
    #header #name_site {
        color:#333;
        text-shadow:2px 2px 5px #444;
        font-size:2em;
    }
    #header #sub_name_site {
        color:#444;
        font-size:1em;
    }
    #header #logo{
        float:right;
        height:125px;
        width:auto;
    }
}

@media all and (max-width: 774px) {
    
    #header #name_site {
        color:#333;
        text-shadow:2px 2px 5px #444;
        font-size:1.5em;
    }
    #header #sub_name_site {
        color:#444;
        font-size:0.75em;
    }
    #header #logo{
        float:right;
        height:100px;
        width:auto;
    }
}

@media all and (max-width: 633px) {
    #header #name_site {
        color:#333;
        text-shadow:2px 2px 5px #444;
        font-size:1em;
    }
    #header #sub_name_site {
        color:#444;
        font-size:0.5em;
    }
    #header #logo{
        float:none;
        height:80px;
        width:auto;
    }
}

@media all and (max-width: 540px) {
    #header {
        text-align:center;
    }
    #header #name_site {
        color:#333;
        text-shadow:2px 2px 5px #444;
        font-size:1em;
    }
    #header #sub_name_site {
        color:#444;
        font-size:0.5em;
    }
    #header #logo{
        float:none;
        height:64px;
        width:auto;
    }
}

@media all and (min-width: 650px) {
    #header nav .menuMobile {
        display:none;
    }
    #header nav ul {
        padding:1px 0px 9px 0px;
        margin:0;
        list-style-type:none;
        height:22px!important;
        background-color:#333;
        border-top:5px solid #fff;
    }
    #header nav ul li {
        padding:5px;
        margin-left:10px;
        float:left;
    }
}

@media all and (max-width: 649px) {
    #header nav .menuMobile {
        font-family: auto, Helvetica, Arial, sans-serif;
        display:block;
        text-align:center;
        color:#6DA406;
        font-weight:bold;
        height:22px;
        vertical-align:middle;
        padding:1px 0px 9px 0px;
        margin:0;
        list-style-type:none;
        background-color:#333;
        border-top:5px solid #fff;
        overflow:hidden;
        width:100%;
        cursor:pointer;
        font-size:1.2em;
    }
    #header nav ul {
        height:0px;
        top:-10px;
        margin:0;
        list-style-type:none;
        background-color:#333;
        border-top:none;
        overflow:hidden;
        width:100%;
        position:relative;
        transition:height 0.5s ease;
        padding:0px;
    }
    #header nav ul li {
        padding:5px;
        border-bottom:1px solid #666;
        text-align:center;
        cursor:pointer;
    }
    #header nav ul li a {
        color:#fff;
        text-decoration:none;
        text-align:center;
        cursor:pointer;
    }
}

#header nav ul li.index {
    margin-right:10px;
    float:right;
}

#header nav ul li a {
    color:#fff;
    text-decoration:none;
}

#header nav ul li {
        background-color:transparent;
        transition: background-color 0.5s linear;
        -o-transition: background-color 0.5s linear;
        -moz-transition: background-color 0.5s linear;
        -ms-transition: background-color 0.5s linear;
        -webkit-transition: background-color 0.5s linear;
        -khtml-transition: background-color 0.5s linear;
}

#header nav ul li a {
        transition: color 0.5s linear;
        -o-transition: color 0.5s linear;
        -moz-transition: color 0.5s linear;
        -ms-transition: color 0.5s linear;
        -webkit-transition: color 0.5s linear;
        -khtml-transition: color 0.5s linear;
}

#header nav ul li:hover {
    background-color:#6DA406;
}

#header nav ul li:hover a{
    color:#333;
}
@media all and (min-width: 776px) {
    #globalmain {
        opacity:1;
        display: table;
        background-color:#efefef;
        table-layout: fixed ;
        width:98%;
        margin:1%;
        box-shadow:0px 0px 5px #222;
    }
}
@media all and (max-width: 775px) {
    #globalmain {
        opacity:1;
        background-color:#efefef;
        width:100%;
        height:100%;
        margin:0px!important;
        padding:0px!important;
        box-shadow:0px 0px 0px #222;
    }
}
@media all and (min-width: 776px) {
    #main {
        display: table-cell;
        <?php if ($SIDEBAR!='none') { ?> width:70%; <?php } ?>
        padding:20px;
        background-color:#eee;
        border:1px solid #e1e1e1;
        margin: 5px;
    }
}
@media all and (max-width: 775px) {
    #main {
        display:block;
        padding:0px;
        background-color:#eee;
        border:1px solid #e1e1e1;
    }
}
#main article img.imgarticle {
    float:right;
    width:25%;
    margin:1%;
    padding:1%;
    border:1px solid #d1d1d1;
}

#main article.article img.pimgarticle {
    max-width:100%;
    width:auto;
    height:auto;
    max-height:210px;
}

@media all and (min-width: 776px) {
    #main article.article {
        float:left;
        width:45.5%;
        min-height:550px;
        max-height:550px;
        overflow:auto;
        margin:1%;
        padding:1%;
        border:1px solid #d1d1d1;
        transition: border 0.5s linear;
        -o-transition: border 0.5s linear;
        -moz-transition: border 0.5s linear;
        -ms-transition: border 0.5s linear;
        -webkit-transition: border 0.5s linear;
        -khtml-transition: border 0.5s linear;
        
    }
}

@media all and (max-width: 775px) {
    #main article.article {
        width:95%;
        min-height:550px;
        max-height:550px;
        overflow:auto;
        margin:1%;
        padding:1%;
        border:1px solid #d1d1d1;
        transition: border 0.5s linear;
        -o-transition: border 0.5s linear;
        -moz-transition: border 0.5s linear;
        -ms-transition: border 0.5s linear;
        -webkit-transition: border 0.5s linear;
        -khtml-transition: border 0.5s linear;
    }
}



#main article.home_page {
        float:left;
        width:100%;
        margin:0%;
        padding:0%;
        border:none;
    }

#main article.article section {
    background-color:#e1e1e1;
    border:1px solid #d1d1d1;
}

#main article h2, #main article h1 {
    margin:0px 0px 10px 0px;
    background-color:#e1e1e1;
    border:1px solid #d1d1d1;
    padding:5px;
}
#main article h2 a{
    color:#333;
    text-decoration:none;
    font-size:18px;
}
#main article footer p{
    color:#666;
    font-style: italic;
    font-size:13px;
    line-height:0.5em;
    text-decoration:none;
}

#main article section pre{
    background-color:#333;
    color:#00FF06;
    width:100%;
    overflow:auto;
}

#main article.article:hover {
    border: 1px solid #B4DF65;
}

h4 {
    color:#fff;
    padding-left:1%;
    background-color:#6DA406;
/*    background-image:linear-gradient(135deg, #000 25%, #000 25%,#fff 25%, #fff 25.5%,#edd400 25.5%, #edd400 90%);*/
}

fieldset {
border-color:transparent;
    transition: border-color 0.5s linear;
    -o-transition: border-color 0.5s linear;
    -moz-transition: border-color 0.5s linear;
    -ms-transition: border-color 0.5s linear;
    -webkit-transition: border-color 0.5s linear;
    -khtml-transition: border-color 0.5s linear;
}

fieldset:hover {
border-color:#6DA406;
}

<?php if ($SIDEBAR!='none') { ?>
@media all and (min-width: 776px) {
    #sidebar {
        background-color:#efefef;
        display: table-cell;
        width:20%;
        padding: 5px;
    }
}
@media all and (max-width: 775px) {
    #sidebar {
        background-color:#efefef;
        width:97%;
        padding: 5px;
        margin-top:10px;
    }
}
@media all and (min-width: 776px) {
    #sidebar section {
        /*position:relative;*/
        background-color:#efefef;
        border:1px solid #e1e1e1;
        width:98%;
        margin-top:-5px;
    }
}
@media all and (max-width: 775px) {
    #sidebar section {
        /*position:relative;*/
        background-color:#efefef;
        border:1px solid #e1e1e1;
        width:100%;
        margin-top:-5px;
    }
}
#sidebar section h3{
    background-color:#e1e1e1;
    border:1px solid #d1d1d1;
    padding:7px;
    margin:20px 5px 5px 5px;
}

#sidebar section ul{
    list-style-type:none;
    margin-left:-30px;
    width:85%;
}
#sidebar section ul li{
    height:25px;
    margin-bottom:5px;
    padding:5px;
    vertical-align:middle;
    background-color:#E1E1E1;
    border: 1px solid #D1D1D1;
    cursor:pointer;
}
#sidebar section ul li a{
    text-decoration:none;
    color:#333;
}
#sidebar section ul li:hover a, #sidebar section ul li a:hover{
    font-weight:bold;
    color:#000;
}
<?php } ?>
</style>
