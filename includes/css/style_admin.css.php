<style type="text/css">
* {
    font-family: sans-serif;
}
body>footer {
    margin-top:50px;
    padding:10px;
    text-align:center;
    background-color:#333;
    color:#eee;
}

#header nav ul {
    padding:1px 0px 9px 0px;;
    margin:0;
    list-style-type:none;
    height:22px;
    background-color:#333;
}

#header nav ul li {
    padding:5px;
    margin-left:10px;
    float:left;
    background-color:#333;
}

#header nav ul li.index {
    margin-right:10px;
    float:right;
}

#header nav ul li a {
    color:#fff;
    text-decoration:none;
    background-color:#333;
}

#header nav ul li:hover {
    background-color:#fff;
}

#header nav ul li:hover a{
    color:#333;
    background-color:#fff;
}

#main {
    padding:20px;
    background-color:#eee;
    border:1px solid #e1e1e1;
    margin: 2%;
}

#main article h2 {
    margin:0px 0px 10px 0px;
    background-color:#e1e1e1;
    border:1px solid #d1d1d1;
    padding:5px;
}
#main article h2 a{
    color:#333;
    text-decoration:none;
}
</style>
