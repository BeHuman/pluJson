<div id="main">
    <?php if (isset($_GET['article']) && isset($_ARTICLES[$_GET['article']]) && $_GET['article'] >= 0) { //ARTICLE 
        include('includes/php/functions/articles.php');
        if (isset($_GET['comment'])) {
            addCommentToArticle($_ARTICLES);
        } ?>
        <article>
                <h2><?php echo $_ARTICLES[$_GET['article']]['title']; ?><?php echo (connected($_ADMINS)?'<a style="float:right;" href="admin.php?module=article&edit='.$_ARTICLES[$_GET['article']]['filename'].'">&eacute;diter</a>':'')?></h2>
                <section>
                <?php echo (!empty($_ARTICLES[$_GET['article']]['img'])?"<img class=\"imgarticle\" src=\"".$_ARTICLES[$_GET['article']]['img']."\">":"") ?>
                <?php echo str_replace("\\", "",$_ARTICLES[$_GET['article']]['chapo'])."<br>"; 
                include('datas/articles/'.$_ARTICLES[$_GET['article']]['filename']); ?>
                </section>
                <footer>
                    <p class="author">Par <?php echo $_ARTICLES[$_GET['article']]['author']; ?> le <?php echo $_ARTICLES[$_GET['article']]['date']; ?></p>
                    <?php if ($_ARTICLES[$_GET['article']]['keywords']!="") { ?><p class="keywords">Mots cl&eacute;s : <?php echo $_ARTICLES[$_GET['article']]['keywords']; ?></p><?php } ?>
                    <?php if ($_ARTICLES[$_GET['article']]['cat']!="") { ?><p class="categories">Cat&eacute;gories : <?php echo $_ARTICLES[$_GET['article']]['cat']; ?></p><?php } ?>
                </footer>
        </article>
        <article>
            <form method="POST" action="?article=<?php echo $_GET['article']; ?>&comment">
                <h4>Ajouter un commentaire</h4>
                <p><label for="nom">Nom : </label><input type="text" id="nom" name="nom" tabindex="1" <?php echo (isset($_POST['nom'])? 'value="'.$_POST['nom'].'"' : '' )?>/></p>
                <p><label for="email">Email : </label><input type="text" id="email" name="email" tabindex="2" <?php echo (isset($_POST['email'])? 'value="'.$_POST['email'].'"' : '' )?>/></p>
                <p><label for="comment">Message :</label><br><textarea id="comment" name="comment" tabindex="4" style="width:100%" rows="8"><?php echo (isset($_POST['comment'])? ''.$_POST['comment'].'' : '' )?></textarea></p>
                <p style="text-align:center;"><label for="antispam_h" style="padding-bottom:25px;">Tapez le nombre</label>&nbsp;&nbsp;<img src="includes/php/functions/capcha.php" alt="Code de vérification" />&nbsp;&nbsp;<span style="margin-top:-25px;"><input type="text" name="antispam_h" id="antispam_h" tabindex="5" /></span></p>
                <div style="text-align:center;"><input style="cursor:pointer;" type="submit" name="envoi" tabindex="6" value="Envoyer" /></div>
            </form><br>
            <?php echo displayCommentsArticle($_ARTICLES); ?>
        </article>
     <?php } else if (isset($_GET['page']) && $_GET['page'] >= 0) { //PAGE ?>
        <article>
                <h2><?php echo $_PAGES[$_GET['page']]['title']; ?></h2>
                <section><?php include('datas/pages/'.$_PAGES[$_GET['page']]['filename']); ?></section>
                <footer>
                    <p class="author">Par <?php echo $_PAGES[$_GET['page']]['author']; ?> le <?php echo $_PAGES[$_GET['page']]['date']; ?></p>
                    <p class="keywords">Mots cl&eacute;s : <?php echo $_PAGES[$_GET['page']]['keywords']; ?></p>
                </footer>
            </article>
    <?php } else if (isset($_GET['categorie']) && $_GET['categorie'] >= 0) { //CATEGORIE 
        $_ARTICLES=array_reverse($_ARTICLES,true);
        $i=count($_ARTICLES)-1; ?>
        <h1><?php echo $_CATEGORIES[$_GET['categorie']]['title']; ?></h1>
        <?php foreach ($_ARTICLES as $article) { 
            if (isset($article['cat']) && !empty($article['cat']) && $article['cat']!="" && preg_match('@'.$_CATEGORIES[$_GET['categorie']]['title'].'@',$article['cat'])) {
        ?>
        <article class="article">
                <h2><a href="?article=<?php echo $i; ?>"><?php echo $article['title']; ?></a></h2>
                <section>
                    <?php echo (!empty($article['img'])?"<a href=\"?article=".$i."\"><img class=\"pimgarticle\" src=\"".$article['img']."\" width=\"100%\" height=\"auto\"></a>":"") ?>
                    <p><?php echo str_replace("\\", "",$article['chapo']); ?>...</p>
                    <p><a href="?article=<?php echo $i; ?>">Lire la suite</a></p>
                </section>
                <footer>
                    <p class="author">Par <?php echo $article['author']; ?> le <?php echo $article['date']; ?></p>
                    <?php if ($article['keywords']!="") { ?><p class="keywords">Mots cl&eacute;s : <?php echo $article['keywords']; ?></p><?php } ?>
                    <?php if ($article['cat']!="") { ?><p class="categories">Cat&eacute;gories : <?php echo $article['cat']; ?></p><?php } ?>
                </footer>
            </article>
    <?php }$i--; }
    } else if (isset($_GET['contact']) && isset($_CONFIGS[0]['mail']) && $_CONFIGS[0]['mail']!="") { //CONTACT  
        include('includes/php/functions/contact.php');?>

        <form id="contact" method="post" action="?contact">
            <h4>Vos coordonnées</h4>
                <p><label for="nom">Nom : </label><input type="text" id="nom" name="nom" tabindex="1" <?php echo (isset($_POST['nom'])? 'value="'.$_POST['nom'].'"' : '' )?>/></p>
                <p><label for="email">Email : </label><input type="text" id="email" name="email" tabindex="2" <?php echo (isset($_POST['email'])? 'value="'.$_POST['email'].'"' : '' )?>/></p>

            <h4>Votre message : </h4>
                <p><label for="objet">Objet : </label><input type="text" id="objet" name="objet" tabindex="3" style="width:50%;" <?php echo (isset($_POST['objet'])? 'value="'.$_POST['objet'].'"' : '' )?>/></p>
                <p><label for="message">Message :</label><br><textarea id="message" name="message" tabindex="4" style="width:100%" rows="8"><?php echo (isset($_POST['message'])? ''.$_POST['message'].'' : '' )?></textarea></p>

            <h4>Antispam</h4>
                <p style="text-align:center;"><label for="antispam_h" style="padding-bottom:25px;">Tapez le nombre</label>&nbsp;&nbsp;<img src="includes/php/functions/capcha.php" alt="Code de vérification" />&nbsp;&nbsp;<span style="margin-top:-25px;"><input type="text" name="antispam_h" id="antispam_h" tabindex="5" /></span></p>
            <div style="text-align:center;"><input style="cursor:pointer;" type="submit" name="envoi" tabindex="6" value="Envoyer" /></div>
        </form>
    <?php } else { //accueil avec liste d'article
        $_ARTICLES=array_reverse($_ARTICLES,true);
        $i=count($_ARTICLES)-1;
        $a=0;
        foreach ($_ARTICLES as $article) {
            if ($a==$_CONFIGS[0]['num_article_home']) break; ?>

            <article class="article">
                <h2><a href="?article=<?php echo $i; ?>"><?php echo $article['title']; ?></a><?php echo (connected($_ADMINS)?'<a style="float:right;" href="admin.php?module=article&edit='.$article['filename'].'">&eacute;diter</a>':'')?></h2>
                <section>
                    <?php echo (!empty($article['img'])?"<a href=\"?article=".$i."\"><img class=\"pimgarticle\" src=\"".$article['img']."\" width=\"100%\" height=\"auto\"></a>":"") ?>
                    <p><?php echo str_replace("\\", "",$article['chapo']); ?>...</p>
                    <p><a href="?article=<?php echo $i; ?>">Lire la suite</a></p>
                </section>
                <footer>
                    <p class="author">Par <?php echo $article['author']; ?> le <?php echo $article['date']; ?></p>
                    <?php if ($article['keywords']!="") { ?><p class="keywords">Mots cl&eacute;s : <?php echo $article['keywords']; ?></p><?php } ?>
                    <?php if ($article['cat']!="") { ?><p class="categories">Cat&eacute;gories : <?php echo $article['cat']; ?></p><?php } ?>
                </footer>
            </article>
    <?php $i--; $a++; }
        foreach ($_PAGES as $page) { 
            if ($page['home']=='oui') {?>
            <article>
                <?php include('datas/pages/'.$page['filename']); ?>
            </article>
        <?php } }
    } ?>
    
    </div>
