<?php if (isset($_GET['article']) && isset($_ARTICLES[$_GET['article']]) && $_GET['article'] >= 0) { 
    $TITLE=(isset($_ARTICLES[$_GET['article']]['title'])?$_ARTICLES[$_GET['article']]['title']:$_CONFIGS[0]['name_site']);
    $DESC=(isset($_ARTICLES[$_GET['article']]['desc'])?$_ARTICLES[$_GET['article']]['desc']:$_CONFIGS[0]['desc']);
    $KEYWORDS=(isset($_ARTICLES[$_GET['article']]['keywords'])?$_ARTICLES[$_GET['article']]['keywords']:$_CONFIGS[0]['keywords']);
    $AUTHOR=(isset($_ARTICLES[$_GET['article']]['author'])?$_ARTICLES[$_GET['article']]['author']:$_CONFIGS[0]['author']);
    $SIDEBAR=(isset($_ARTICLES[$_GET['article']]['sidebar'])?$_ARTICLES[$_GET['article']]['sidebar']:$_CONFIGS[0]['sidebar']);
} else if (isset($_GET['page']) && isset($_PAGES[$_GET['page']]) && $_GET['page'] >= 0) {
    $TITLE=(isset($_PAGES[$_GET['page']]['title'])?$_PAGES[$_GET['page']]['title']:$_CONFIGS[0]['name_site']);
    $DESC=(isset($_PAGES[$_GET['page']]['desc'])?$_PAGES[$_GET['page']]['desc']:$_CONFIGS[0]['desc']);
    $KEYWORDS=(isset($_PAGES[$_GET['page']]['keywords'])?$_PAGES[$_GET['page']]['keywords']:$_CONFIGS[0]['keywords']);
    $AUTHOR=(isset($_PAGES[$_GET['page']]['author'])?$_PAGES[$_GET['page']]['author']:$_CONFIGS[0]['author']);
    $SIDEBAR=(isset($_PAGES[$_GET['page']]['sidebar'])?$_PAGES[$_GET['page']]['sidebar']:$_CONFIGS[0]['sidebar']);
} else {
    $TITLE=$_CONFIGS[0]['name_site'];
    $DESC=$_CONFIGS[0]['desc'];
    $KEYWORDS=$_CONFIGS[0]['keywords'];
    $AUTHOR=$_CONFIGS[0]['author'];
    $SIDEBAR=$_CONFIGS[0]['sidebar'];
} ?>
<title><?php echo $TITLE; ?></title>
<meta charset="UTF-8">
<meta name="description" content="<?php echo $DESC; ?>">
<meta name="keywords" content="<?php echo $KEYWORDS; ?>">
<meta name="author" content="<?php echo $AUTHOR; ?>">
<link rel="icon" href="includes/imgs/favicon.ico" />
<link rel="icon" type="image/png" href="includes/imgs/favicon.png" />
<?php
include('includes/js/main.js.php');
include('includes/css/style.css.php');
?>

