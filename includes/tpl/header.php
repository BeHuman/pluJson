<div id="header">
    <img id="logo" src="includes/imgs/logo.png">
    <h1 id="name_site"><?php echo $_CONFIGS[0]['name_site']; ?></h1>
    <?php echo ($_CONFIGS[0]['sub_name_site']!=""?"<p id='sub_name_site'>".$_CONFIGS[0]['sub_name_site']."</p>":""); ?>

    <nav>
    <?php $numMenu=(count($_MENUS)+2)*27; ?>
        <div class="menuMobile" onclick="if (document.getElementById('menuUL').style.height=='0px' || document.getElementById('menuUL').style.height=='') {document.getElementById('menuUL').style.height='<?php echo $numMenu; ?>px';} else {document.getElementById('menuUL').style.height='0px'; }">Menu</div>
        <ul id="menuUL">
            <?php foreach ($_MENUS as $menu) { ?>
            <li <?php echo ($menu['styleli']!=""?'style="'.$menu['styleli'].'"':''); ?>><a <?php echo ($menu['stylelink']!=""?'style="'.$menu['stylelink'].'"':''); ?> href="<?php echo $menu['link']; ?>" <?php echo ($menu['target']!=""?'target="'.$menu['target'].'"':''); ?> ><?php echo $menu['title']; ?></a></li>
            <?php }
            if ($_CONFIGS[0]['menumail']=='yes' && isset($_CONFIGS[0]['mail']) && $_CONFIGS[0]['mail']!="") { ?>
                <li <?php echo ($_CONFIGS[0]['styleli_menutitlemail']!=""?'style="'.$_CONFIGS[0]['styleli_menutitlemail'].'"':''); ?>><a href="?contact" <?php echo ($_CONFIGS[0]['stylelink_menutitlemail']!=""?'style="'.$_CONFIGS[0]['stylelink_menutitlemail'].'"':''); ?>><?php echo $_CONFIGS[0]['menutitlemail']; ?></a></li>
            <?php } ?>
        </ul>
    </nav>
</div>
