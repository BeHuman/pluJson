<?php
    $_CONTENT= array();
    if (isset($_GET['module'])) {
        if ($_GET['module']=='article') {
            $_CONTENT['title']=$_LANGS['admin_configs'].' '.$_LANGS['admin_articles'];
        } else if ($_GET['module']=='categorie') {
            $_CONTENT['title']=$_LANGS['admin_configs'].' '.$_LANGS['admin_categories'];
        } else if ($_GET['module']=='page') {
            $_CONTENT['title']=$_LANGS['admin_configs'].' '.$_LANGS['admin_pages'];
        } else if ($_GET['module']=='file') {
            $_CONTENT['title']=$_LANGS['admin_configs'].' '.$_LANGS['admin_files'];
        } else if ($_GET['module']=='config') {
            $_CONTENT['title']=$_LANGS['admin_config'];
        } else if ($_GET['module']=='admin') {
            $_CONTENT['title']=$_LANGS['admin_admins'];
        } else if ($_GET['module']=='menu') {
            $_CONTENT['title']=$_LANGS['admin_configs'].' '.$_LANGS['admin_menus'];
        } else {
            $_CONTENT['title']="R&eacute;capitulatif";
        }
    } else {
        $_CONTENT['title']="R&eacute;capitulatif";
    }
?>
<div id="main">
    <article>
        <h2><?php echo $_CONTENT['title']; ?></h2>
        <section>
        <?php
            if (isset($_GET['action']) && $_GET['action']=='connect') {
                if ( (isset($_POST['user']) && isset($_POST['pwd']))){
                    foreach ($_ADMINS as $item) {
                        if ($item['user']==hash('sha256',$_POST['user']) && $item['pwd']==hash('sha256',$_POST['pwd'])) {
                            $_SESSION['user']=hash('sha256',$_POST['user']);
                            $_SESSION['pwd']=hash('sha256',$_POST['pwd']);
                        }
                    }
                }
            }
            if (connected($_ADMINS)) {
                $_CONTENT= array();
                if (isset($_GET['module'])) {
                    if ($_GET['module']=='article') {
                        if (isset($_GET['submodule']) && $_GET['submodule']=='comments') {
                            if (isset($_GET['del'])) {
                                deleteCommentsArticle();
                                header('Location: admin.php?module=article&edit='.$_GET['del']);
                            } else if (isset($_GET['valid'])) {
                                validCommentsArticle();
                                header('Location: admin.php?module=article&edit='.$_GET['valid']);
                            } else if (isset($_GET['invalid'])) {
                                invalidCommentsArticle();
                                header('Location: admin.php?module=article&edit='.$_GET['invalid']);
                            } else {
                                listArticles($_ARTICLES);
                            }
                        } else {
                            if (isset($_GET['edit'])) {
                                editArticle($_ARTICLES, $_GET['edit'], $_CONFIGS);
                                $_ARTICLES = json_decode(file_get_contents('datas/articles.json'), true);
                                listCommentsArticle($_ARTICLES);
                                echo "<h2>Liste des articles</h2>";
                                listArticles($_ARTICLES);
                            } else if (isset($_GET['del'])) {
                                deleteArticle($_ARTICLES, $_GET['del']);
                                $_ARTICLES = json_decode(file_get_contents('datas/articles.json'), true);
                                listArticles($_ARTICLES);
                            } else {
                                listArticles($_ARTICLES);
                            }
                        }
                    } else if ($_GET['module']=='menu') {
                        if (isset($_GET['edit'])) {
                            editMenu($_MENUS, $_GET['edit'], $_CONFIGS, $_GET['timestamp']);
                            $_MENUS = json_decode(file_get_contents('datas/menus.json'), true);
                            listMenus($_MENUS);
                        } else if (isset($_GET['del'])) {
                            deleteMenu($_MENUS, $_GET['del']);
                            $_MENUS = json_decode(file_get_contents('datas/menus.json'), true);
                            listMenus($_MENUS);
                        } else {
                            listMenus($_MENUS);
                        }
                    } else if ($_GET['module']=='page') {
                        if (isset($_GET['edit'])) {
                            editPage($_PAGES, $_GET['edit'], $_CONFIGS);
                            $_PAGES = json_decode(file_get_contents('datas/pages.json'), true);
                            listPages($_PAGES);
                        } else if (isset($_GET['del'])) {
                            deletePage($_PAGES, $_GET['del']);
                            $_PAGES = json_decode(file_get_contents('datas/pages.json'), true);
                            listPages($_PAGES);
                        } else {
                            listPages($_PAGES);
                        }
                    } else if ($_GET['module']=='file') {
                        if (isset($_GET['edit'])) {
                            editFile($__FILES, $_GET['edit'], $_CONFIGS);
                            $__FILES = json_decode(file_get_contents('datas/files.json'), true);
                            listFiles($__FILES);
                        } else if (isset($_GET['del'])) {
                            deleteFile($__FILES, $_GET['del']);
                            $__FILES = json_decode(file_get_contents('datas/files.json'), true);
                            listFiles($__FILES);
                        } else {
                            listFiles($__FILES);
                        }
                    } else if ($_GET['module']=='config') {
                        editConfig($_CONFIGS);
                        echo '<h2>Informations PHP</h2>';
                        phpinfo();
                    } else if ($_GET['module']=='admin') {
                        if (isset($_GET['edit'])) {
                            editAdmin($_ADMINS, $_GET['edit'], $_CONFIGS);
                            $_ADMINS = json_decode(file_get_contents('datas/admins.json'), true);
                            listAdmins($_ADMINS);
                        } else if (isset($_GET['del'])) {
                            deleteAdmin($_ADMINS, $_GET['del']);
                            $_ADMINS = json_decode(file_get_contents('datas/admins.json'), true);
                            listAdmins($_ADMINS);
                        } else {
                            listAdmins($_ADMINS);
                        }
                    } else if ($_GET['module']=='categorie') {
                        if (isset($_GET['edit'])) {
                            editCategorie($_CATEGORIES, $_GET['edit'], $_CONFIGS);
                            $_CATEGORIES = json_decode(file_get_contents('datas/categories.json'), true);
                            listCategories($_CATEGORIES);
                        } else if (isset($_GET['del'])) {
                            deleteCategorie($_CATEGORIES, $_GET['del']);
                            $_CATEGORIES = json_decode(file_get_contents('datas/categories.json'), true);
                            listCategories($_CATEGORIES);
                        } else {
                            listCategories($_CATEGORIES);
                        }
                    } else {
                        echo "<p>Nombre d'articles : <a href='?module=article'>",count($_ARTICLES),"</a></p>",
                                "<p>Nombre de pages : <a href='?module=page'>",count($_PAGES),"</a></p>",
                                "<p>Nombre de cat&eacute;gories : <a href='?module=categorie'>",count($_CATEGORIES),"</a></p>",
                                "<p>Nombre de menus : <a href='?module=menu'>",count($_MENUS),"</a></p>",
                                "<p>Nombre de fichiers : <a href='?module=menu'>",count($__FILES),"</a></p>",
                                "<p>Nombre d'administrateurs : <a href='?module=admin'>",count($_ADMINS),"</a></p>",
                                "<h2>Configuration</h2>";
                        editConfig($_CONFIGS);
                        echo '<h2>Informations PHP</h2>';
                        phpinfo();
                    }
                } else {
                    echo "<p>Nombre d'articles : <a href='?module=article'>",count($_ARTICLES),"</a></p>",
                            "<p>Nombre de pages : <a href='?module=page'>",count($_PAGES),"</a></p>",
                            "<p>Nombre de cat&eacute;gories : <a href='?module=categorie'>",count($_CATEGORIES),"</a></p>",
                            "<p>Nombre de menus : <a href='?module=menu'>",count($_MENUS),"</a></p>",
                            "<p>Nombre de fichiers : <a href='?module=menu'>",count($__FILES),"</a></p>",
                            "<p>Nombre d'administrateurs : <a href='?module=admin'>",count($_ADMINS),"</a></p>",
                            "<h2>Configuration</h2>";
                    editConfig($_CONFIGS);
                    echo '<h2>Informations PHP</h2>';
                    phpinfo();
                }
            } else { ?>
                <form method="POST" action="admin.php?action=connect" style="text-align:center;">
                    <h3>Identification</h3>
                    <p><label>Utilisateur : </label><input type="text" name="user" value=""></p>
                    <p><label>Mot de passe : </label><input type="password" name="pwd" value=""></p>
                    <button>Valider</button>
                </form>
            <?php }
        ?>
        </section>
        <footer></footer>
    </article>
</div>
