<div id="header">
    <h1><?php echo $_LANGS['admin_title']; ?></h1>
    <nav>
        <ul>
            <li><a href="admin.php"><?php echo $_LANGS['admin_home']; ?></a></li>
            <li><a href="admin.php?module=article"><?php echo $_LANGS['admin_articles']; ?></a></li>
            <li><a href="admin.php?module=page"><?php echo $_LANGS['admin_pages']; ?></a></li>
            <li><a href="admin.php?module=categorie"><?php echo $_LANGS['admin_categories']; ?></a></li>
            <li><a href="admin.php?module=menu"><?php echo $_LANGS['admin_menus']; ?></a></li>
            <li><a href="admin.php?module=file"><?php echo $_LANGS['admin_files']; ?></a></li>
            <li><a href="admin.php?module=admin"><?php echo $_LANGS['admin_admins']; ?></a></li>
            <li><a href="admin.php?module=config"><?php echo $_LANGS['admin_config']; ?></a></li>
            <li class="index"><a href="index.php" style="color:red;"><strong><?php echo $_LANGS['admin_index']; ?></strong></a></li>
        </ul>
    </nav>
</div>
