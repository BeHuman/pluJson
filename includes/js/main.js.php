<script type="text/javascript">
<!--
if (readCookie('wrapperBackground')!=null) var oldrdm=readCookie('wrapperBackground');
else var oldrdm=-1;

function rdmf(rdm) {
    if (rdm==0) {
        return "linear-gradient(-45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 30%, transparent 30%)";
    } else if (rdm==1) {
        return "linear-gradient(45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 30%, transparent 30%)";
    } else if (rdm==2) {
        return "linear-gradient(45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 90%, #fff 90%, #fff 90.5%, #000 90.5%)";
    } else if (rdm==3) {
        return "linear-gradient(-45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 90%, #fff 90%, #fff 90.5%, #000 90.5%)";
    } else if (rdm==4) {
        return "linear-gradient(45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%),linear-gradient(-135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%)";
    } else if (rdm==5) {
        return "linear-gradient(-45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%),linear-gradient(135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%)";
    } else if (rdm==6) {
        return "linear-gradient(-135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 30%, transparent 30%)";
    } else if (rdm==7) {
        return "linear-gradient(135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 30%, transparent 30%)";
    } else if (rdm==8) {
        return "linear-gradient(135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 90%, #fff 90%, #fff 90.5%, #000 90.5%)";
    } else if (rdm==9) {
        return "linear-gradient(-135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 90%, #fff 90%, #fff 90.5%, #000 90.5%)";
    } else if (rdm==10) {
        return "linear-gradient(135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%),linear-gradient(-45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%)";
    } else if (rdm==11) {
        return "linear-gradient(-135deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%),linear-gradient(45deg, #000 10%, #000 10%,#fff 10%, #fff 10.5%,#edd400 10.5%, #edd400 51%, transparent 51%)";
    }
}
function loadBackground() {
    if (readCookie('wrapperBackground')!=null) var rdm=readCookie('wrapperBackground');
    else var rdm=Math.floor(Math.random() * (11 - 0 + 0)) + 0;
    document.getElementById('wrapperBackground').style.backgroundImage=rdmf(rdm);
    if (readCookie('wrapperBackground')!=null) eraseCookie('wrapperBackground');
    else document.getElementById('wrapperBackground').style.opacity="0";
    setTimeout(function() {document.getElementById('wrapperBackground').style.opacity="1";},250);
    createCookie('wrapperBackground', rdm, 1);
    var intervalBackground=setInterval( function () {
        var rdm=Math.floor(Math.random() * (11 - 0 + 0)) + 0;
        console.log('old='+oldrdm+' > rdm='+rdm);
        while (rdm==oldrdm && oldrdm > -1) {
            rdm=Math.floor(Math.random() * (11 - 0 + 0)) + 0;
        }
        document.getElementById('wrapperBackground').style.opacity="0";
        oldrdm=rdm;
        eraseCookie('wrapperBackground');
        createCookie('wrapperBackground', rdm, 1);
        setTimeout(function() {document.getElementById('wrapperBackground').style.backgroundImage=rdmf(rdm);},2000);
        setTimeout(function() {document.getElementById('wrapperBackground').style.opacity="1";},2010);
    }, 10000);
}

/* gestion des cookies */
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
		expires = "; expires=0";
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
-->
</script>
