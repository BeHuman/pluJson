<?php
//PAGES
function listPages($datas) {
    echo "<p><a style=\"margin-left:25px\" href=\"?module=page&edit=",hash('sha256', date('Y-m-d H:i:s')),".php\">Ajouter une page</a></p>";
    echo "<ul style=\"list-style-type:none;width:92%\">";
    $back=false;
    $i=0;
    foreach ($datas as $item) {
        echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
        echo "<a href=\"?module=page&edit=",$item['filename'],"\" style=\"color:",($back?"#333;":"#eee;"),"\">",$item['title'],"</a> par <strong>",$item['author'],"</strong> le ",$item['date'],"<a href=\"?module=page&del=",$item['filename'],"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir suprimmer la page ".$item['title']." ?') ) { return true; } else { return false; }\">Supprimer</a><a href=\"index.php?page=".$i."\" target=\"_BLANK\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\">Voir</a></li>";
        if ($back) $back=false;
        else $back=true;
        $i++;
    }
    echo "</ul>",
    "<p><a style=\"margin-left:25px\" href=\"?module=page&edit=",hash('sha256', date('Y-m-d H:i:s')),".php\">Ajouter une page</a></p>";
}

function displayPage($datas, $id) {
    //FIXME
}

function getIDPageByFilename($datas, $filename) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['filename']==$filename) {
            return $i;
        }
        $i++;
    }
    return -1;
}
function getIDPageByTitle($datas, $title) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['title']==$title) {
            return $i;
        }
        $i++;
    }
    return -1;
}


function editPage($datas, $filename, $_CONFIGS=array()) {
    $ID=getIDPageByFilename($datas, $filename);
    if (isset($_POST['title']) && isset($_POST['author']) && isset($_POST['date']) && isset($_POST['content']) && isset($_POST['metadesc']) && isset($_POST['keywords']) && isset($_POST['sidebar']) && isset($_POST['home']) && isset($_POST['position'])) {
        if ($ID>=0) $cdatas=$ID;
        else $cdatas=count($datas);
        $datas[$cdatas]['title']=$_POST['title'];
        $datas[$cdatas]['filename']=$filename;
        $datas[$cdatas]['author']=$_POST['author'];
        $datas[$cdatas]['date']=$_POST['date'];
        $datas[$cdatas]['metadesc']=$_POST['metadesc'];
        $datas[$cdatas]['keywords']=$_POST['keywords'];
        $datas[$cdatas]['sidebar']=$_POST['sidebar'];
        $datas[$cdatas]['home']=$_POST['home'];
        $datas[$cdatas]['position']=$_POST['position'];
        $position=array();
        $title=array();
        foreach ($datas as $key => $row) {
            $position[$key]  = $row['position'];
            $title[$key]  = $row['title'];
        }
        array_multisort($position, SORT_ASC,$title, SORT_ASC, $datas);
        $datas_=array();
        $a=0;
        foreach ($datas as $item) {
            $datas_[$a]=$item;
            $datas_[$a]['position']=$a;
            $a++;
        }
        $fp = fopen('datas/pages.json', 'w+');
        fwrite($fp, json_encode($datas_,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
        $fp = fopen('datas/pages/'.$filename, 'w+');
        fwrite($fp, str_replace("\\", "",$_POST['content']));
        fclose($fp);
    } else {
        $ID=getIDPageByFilename($datas, $filename);
        //display form
        //str_replace("\\", "", $chaine);
        echo "<form method=\"POST\"><p>Titre : <input name=\"title\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['title']:"")."\"></p>",
        "<p>Auteur : <input name=\"author\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['author']:$_CONFIGS[0]['author'])."\"></p>",
        "<p>Date : <input name=\"date\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['date']:date('Y-m-d H:i:s'))."\"></p>",
        "<p>Contenu de la page : <textarea id=\"content\" name=\"content\" style=\"width:100%;height:200px\">",
        (is_file('datas/pages/'.$filename)?htmlspecialchars(file_get_contents('datas/pages/'.$filename)):''),
        "</textarea></p>",
        "<p>Pr&eacute;visualisation :",
        "<div id=\"previewContent\" style=\"max-height:200px;overflow:auto;border:3px solid #333;\"></div></p><br>",
        "<script type=\"text/javascript\">
        update();
        var old=\"\";
        function update()
        {
          var textarea = document.getElementById('content');
          var preview = document.getElementById('previewContent');
          if (old != textarea.value) {
            old = textarea.value;
            preview.innerHTML=old;
          }
          window.setTimeout(update, 150);
        }
        </script>",
        "<p>Meta description : <input name=\"metadesc\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['metadesc']:"")."\"></p>",
        "<p>Mots cl&eacute;s : <input name=\"keywords\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['keywords']:"")."\"></p>",
        "<p>Position SideBar : <select name=\"sidebar\"><option value='none' ".($ID>=0 && ($datas[$ID]['sidebar']!='left' || $datas[$ID]['sidebar']!='right') ?"selected":"").">Aucune</option><option value='right' ".($ID>=0 && $datas[$ID]['sidebar']=='right' ?"selected ":"").">Droite</option><option value='left' ".($ID>=0 && $datas[$ID]['sidebar']=='left' ?"selected ":"").">Gauche</option></select></p>",
        "<p>Afficher à l'accueil : <select name=\"home\"><option value='none' ".($ID>=0 && $datas[$ID]['home']!='oui' ?"selected":"").">Non</option><option value='oui' ".($ID>=0 && $datas[$ID]['home']=='oui' ?"selected ":"").">Oui</option></select></p>",
        "<p>Position : <input name=\"position\" type=\"number\" value=\"".($ID>=0?(isset($datas[$ID]['position'])?$datas[$ID]['position']:count($datas)):count($datas))."\"></p>",
        "<p><button onclick=\"window.location.href='admin.php?module=page';return false;\">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;<button>Sauvegarder</button></p>",
        "</form>";
    }
}

function deletePage($datas, $filename) {
    $ID=getIDPageByFilename($datas, $filename);
    unlink('datas/pages/'.$filename);
    unset($datas[$ID]);
    $datas2=array();
    foreach ($datas as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/pages.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

?>
