<?php
/*
    ********************************************************************************************
    CONFIGURATION
    ********************************************************************************************
*/
// destinataire est votre adresse mail. Pour envoyer à plusieurs à la fois, séparez-les par une virgule
$destinataire = $_CONFIGS[0]['mail'];
 
// copie ? (envoie une copie au visiteur)
$copie = 'oui'; // 'oui' ou 'non'
 
// Messages de confirmation du mail
$message_envoye = "<span style='color:green;'>Votre message nous est bien parvenu !</span>";
$message_non_envoye = "<span style='color:red;'>L'envoi du mail a échoué, veuillez réessayer SVP.</span>";
 
// Messages d'erreur du formulaire
$message_erreur_formulaire = "<span style='color:red;'>Vous devez d'abord <a href=\"?contact\">envoyer le formulaire</a>.</span>";
$message_formulaire_invalide = "<span style='color:red;'>Vérifiez que tous les champs soient bien remplis et que l'email soit sans erreur.</span>";
$message_formulaire_invalide_spam = "<span style='color:red;'>Le code AntiSpam est incorrect.</span>";

/*
    ********************************************************************************************
    FIN DE LA CONFIGURATION
    ********************************************************************************************
*/
if (isset($_POST['envoi'])) {
    // formulaire envoyé, on récupère tous les champs.
    $nom     = (isset($_POST['nom']))     ? Rec($_POST['nom'])     : '';
    $email   = (isset($_POST['email']))   ? Rec($_POST['email'])   : '';
    $objet   = (isset($_POST['objet']))   ? Rec($_POST['objet'])   : '';
    $message = (isset($_POST['message'])) ? Rec($_POST['message']) : '';
 
    // On va vérifier les variables et l'email ...
    $email = (IsEmail($email)) ? $email : ''; // soit l'email est vide si erroné, soit il vaut l'email entré
 
    if (($nom != '') && ($email != '') && ($objet != '') && ($message != '') && $_POST['antispam_h']==$_SESSION['aleat_nbr']) {
        unset($_SESSION['aleat_nbr']);
        // les 4 variables sont remplies, on génère puis envoie le mail
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'From:'.$nom.' <'.$email.'>' . "\r\n" .
                'Reply-To:'.$email. "\r\n" .
                (isset($_CONFIGS[0]['mailhide']) && $_CONFIGS[0]['mailhide']!="" ?'Bcc: '.$_CONFIGS[0]['mailhide']."\r\n":'').
                'Content-Type: text/plain; charset="utf-8"; DelSp="Yes"; format=flowed '."\r\n" .
                'Content-Disposition: inline'. "\r\n" .
                'Content-Transfer-Encoding: 7bit'." \r\n" .
                'X-Mailer:PHP/'.phpversion();
    
        // envoyer une copie au visiteur ?
        if ($copie == 'oui') {
            $cible = $destinataire.(isset($_CONFIGS[0]['mailcopy']) && $_CONFIGS[0]['mailcopy']!=""?','.$_CONFIGS[0]['mailcopy']:"");
        } else {
            $cible = $destinataire;
        }
 
        // Remplacement de certains caractères spéciaux
        $message = str_replace("&#039;","'",$message);
        $message = str_replace("&#8217;","'",$message);
        $message = str_replace("&quot;",'"',$message);
        $message = str_replace('<br>','',$message);
        $message = str_replace('<br />','',$message);
        $message = str_replace("&lt;","<",$message);
        $message = str_replace("&gt;",">",$message);
        $message = str_replace("&amp;","&",$message);
 
        // Envoi du mail
        if (mail($cible, $objet, $message, $headers)) {
            unset($_POST);
            echo '<p>'.$message_envoye.'</p>'."\n";
        } else {
            echo '<p>'.$message_non_envoye.'</p>'."\n";
        }
    } else {
        if ($_POST['antispam_h']==$_SESSION['aleat_nbr']) {
            // une des 3 variables (ou plus) est vide ...
            echo '<p>'.$message_formulaire_invalide."\n";
        } else {
            echo '<p>'.$message_formulaire_invalide_spam."\n";
        }
        
    }
} // fin du if (!isset($_POST['envoi']))
?>
