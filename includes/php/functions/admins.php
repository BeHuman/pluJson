<?php
//ADMINS
function listAdmins($datas) {
    echo "<p><a style=\"margin-left:25px\" href=\"?module=admin&edit=",hash('sha256', date('Y-m-d H:i:s')),".php\">Ajouter un administrateur</a></p>";
    echo "<ul style=\"list-style-type:none;width:92%\">";
    $back=false;
    foreach ($datas as $item) {
        echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
        echo "<a href=\"?module=admin&edit=",$item['user'],"\" style=\"color:",($back?"#333;":"#eee;"),"\">",$item['name'],"</a> <a href=\"?module=admin&del=",urlencode($item['user']),"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir supprimer l&apos;administrateur ".$item['name']." ?') ) { return true; } else { return false; }\">Supprimer</a></li>";
        if ($back) $back=false;
        else $back=true;
    }
    echo "</ul>",
    "<p><a style=\"margin-left:25px\" href=\"?module=admin&edit=new\">Ajouter un administrateur</a></p>";
}

function getIDAdmin($datas, $user) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['user']==urldecode($user)) {
            return $i;
        }
        $i++;
    }
    return -1;
}

function editAdmin($datas, $user, $_CONFIGS=array()) {
    
    if (isset($_POST['name']) && isset($_POST['user']) && isset($_POST['pwd']) && isset($_POST['pwd2']) && ($_POST['pwd']==$_POST['pwd2']) && $_POST['pwd']!="" && $_POST['pwd2']!="") {
        $ID=getIDAdmin($datas, $_POST['user']);
        if ($ID>=0) $cdatas=$ID;
        else $cdatas=count($datas);
        $datas[$cdatas]['name']=$_POST['name'];
        $datas[$cdatas]['user']=hash('sha256',$_POST['user']);
        $datas[$cdatas]['pwd']=hash('sha256',$_POST['pwd']);
        $fp = fopen('datas/admins.json', 'w+');
        fwrite($fp, json_encode($datas,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
        echo "<p style='color:green'>Administrateur enregistr&eacute;</p>";
    } else {
        $ID=getIDAdmin($datas, (isset($_POST['user'])?$_POST['user']:$user));
        //display form
        if ($_POST['pwd']!="" && $_POST['pwd2']!="") {
            echo "<p style='color:red'>Le mot de passe ne doit pas &ecric;tre vide</p>";
        }
        echo "<form method=\"POST\"><p>Nom : <input name=\"name\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['name']:"")."\"></p>",
            "<p>Nom d'utilisateur : <input name=\"user\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['user']:"")."\"></p>",
            "<p>Mot de passe : <input name=\"pwd\" type=\"password\" value=\"\"></p>",
            "<p>Verification du mot de passe : <input name=\"pwd2\" type=\"password\" value=\"\"></p>",
            "<p><button onclick=\"window.location.href='admin.php?module=admin';return false;\">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;<button>Sauvegarder</button></p>",
            "</form>";
    }
}

function deleteAdmin($datas, $user) {
    $ID=getIDAdmin($datas, $user);
    unset($datas[$ID]);
    $datas2=array();
    foreach ($datas as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/admins.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

?>
