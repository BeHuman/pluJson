<?php
//ARTICLES
function listArticles($datas) {
    echo "<p><a style=\"margin-left:25px\" href=\"?module=article&edit=",hash('sha256', date('Y-m-d H:i:s')),".php\">Ajouter un article</a></p>";
    echo "<ul style=\"list-style-type:none;width:92%\">";
    $back=false;
    $i=0;
    foreach ($datas as $item) {
        echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
        echo "<a href=\"?module=article&edit=",$item['filename'],"\" style=\"color:",($back?"#333;":"#eee;"),"\">",$item['title'],"</a> par <strong>",$item['author'],"</strong> le ",$item['date'],"<a href=\"?module=article&del=",$item['filename'],"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir suprimmer l\\'article ".$item['title']." ?') ) { return true; } else { return false; }\">Supprimer</a><a href=\"index.php?article=".$i."\" target=\"_BLANK\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\">Voir</a></li>";
        if ($back) $back=false;
        else $back=true;
        $i++;
    }
    echo "</ul>",
    "<p><a style=\"margin-left:25px\" href=\"?module=article&edit=",hash('sha256', date('Y-m-d H:i:s')),".php\">Ajouter un article</a></p>";
}

function listCommentsArticle($datas) {
    $ID=getIDbyFilename($datas, $_GET['edit']);
    if (file_exists('datas/comments/'.$_GET['edit'].'.json')) {
        $_COMMENT=json_decode(file_get_contents('datas/comments/'.$_GET['edit'].'.json'), true);
        if (count($_COMMENT)>0) {
            echo "<h2>Liste des commentaires de l'article</h2>";
            echo "<ul style=\"list-style-type:none;width:92%\">";
            $back=false;
            $i=0;
            
            foreach ($_COMMENT as $item) {
                echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
                echo ($item['valid']?'':'<s>'),"le ",$item['date']," par <strong>",$item['name'],"</strong> <a href=\"?module=article&submodule=comments&id=",$i,"&del=",$_GET['edit'],"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir supprimer le commentaire  ?') ) { return true; } else { return false; }\">Supprimer</a><a href=\"?module=article&submodule=comments&id=",$i,"&",($item['valid']?'invalid':'valid'),"=",$_GET['edit'],"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir ",($item['valid']?'Invalider':'valider')," le commentaire  ?') ) { return true; } else { return false; }\">",($item['valid']?'Invalider':'valider'),"</a>",($item['valid']?'':'</s>'),"</li>";
                if ($back) $back=false;
                else $back=true;
                echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
                echo $item['comment'],"</li>";
                if ($back) $back=false;
                else $back=true;
                $i++;
            }
            echo "</ul><br><hr>";
        }
    }
}

function displayCommentsArticle($datas) {
    $out= "";
    if (file_exists('datas/comments/'.$datas[$_GET['article']]['filename'].'.json')) {
        $_COMMENT=json_decode(file_get_contents('datas/comments/'.$datas[$_GET['article']]['filename'].'.json'), true);
        if (count($_COMMENT)>0) {
            $e=0;
            foreach ($_COMMENT as $item) {
                if ($item['valid']) {
                    $out.= "<h4 style='padding:5px;'>par <strong>".$item['name']."</strong> "." <em>le ".$item['date']."</em></h4>";
                    $out.= "<div style='background-color:#fff;margin-top:-20px;'>".$item['comment']."</div>";
                    $e++;
                }
            }
            $out2= "<h2>Commentaires</h2>";
            return ($e>0?$out2:'').$out;
        }
    }
    return $out;
}

function deleteCommentsArticle() {
    $ID=$_GET['id'];
    $_COMMENT=json_decode(file_get_contents('datas/comments/'.$_GET['del'].'.json'), true);
    unset($_COMMENT[$ID]);
    $datas2=array();
    foreach ($_COMMENT as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/comments/'.$_GET['del'].'.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

function validCommentsArticle($datas, $filename) {
    $ID=$_GET['id'];
    $_COMMENT=json_decode(file_get_contents('datas/comments/'.$_GET['valid'].'.json'), true);
    $_COMMENT[$ID]['valid']=true;
    $datas2=array();
    foreach ($_COMMENT as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/comments/'.$_GET['valid'].'.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

function invalidCommentsArticle($datas, $filename) {
    $ID=$_GET['id'];
    $_COMMENT=json_decode(file_get_contents('datas/comments/'.$_GET['invalid'].'.json'), true);
    $_COMMENT[$ID]['valid']=false;
    $datas2=array();
    foreach ($_COMMENT as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/comments/'.$_GET['invalid'].'.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

function getIDbyFilename($datas, $filename) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['filename']==$filename) {
            return $i;
        }
        $i++;
    }
    return -1;
}
function getIDbyTitle($datas, $title) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['title']==$title) {
            return $i;
        }
        $i++;
    }
    return -1;
}


function addCommentToArticle($datas) {
    $message_envoye = "<span style='color:green;'>Votre commentaire nous est bien parvenu ! Il sera affich&eacute; apr&egrave;s sa mod&eacute;ration.</span>";
    $message_non_envoye = "<span style='color:red;'>L'envoi du commentaire a échoué, veuillez réessayer SVP.</span>";
    // Messages d'erreur du formulaire
    $message_formulaire_invalide = "<span style='color:red;'>Vérifiez que tous les champs soient bien remplis et que l'email soit sans erreur.</span>";
    $message_formulaire_invalide_spam = "<span style='color:red;'>Le code AntiSpam est incorrect.</span>";
    
    $ID=$_GET['article'];
    if (isset($_POST['nom']) && isset($_POST['email']) && IsEmail($_POST['email']) && isset($_POST['comment']) && $_POST['antispam_h']==$_SESSION['aleat_nbr']) {
        unset($_SESSION['aleat_nbr']);
        if ($ID>=0) $carticle=$ID;
        else $carticle=count($datas);
        $filename=$datas[$carticle]['filename'];
        $date=date('Y-m-d');
        if (file_exists('datas/comments/'.$filename.'.json')) {
            $_COMMENT=json_decode(file_get_contents('datas/comments/'.$filename.'.json'), true);
            $ccomment=count($_COMMENT);
        } else {
            $_COMMENT=array();
            $ccomment=0;
        }
        $_COMMENT[$ccomment]['name']=Rec($_POST['nom']);
        $_COMMENT[$ccomment]['email']=$_POST['email'];
        $_COMMENT[$ccomment]['date']=$date;
        $_COMMENT[$ccomment]['valid']=false;
        $_COMMENT[$ccomment]['comment']=Rec($_POST['comment']);
        $fp = fopen('datas/comments/'.$filename.'.json', 'w+');
        fwrite($fp, json_encode($_COMMENT,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
        
        echo $message_envoye;
        echo "<script type='text/javascript'>window.history.pushState('".$datas[$carticle]['title']."', '".$datas[$carticle]['title']."', '?article=".$ID."');
</script>";
        unset($_POST);
    } else {
        if ($_POST['antispam_h']==$_SESSION['aleat_nbr']) {
            echo '<p>'.$message_formulaire_invalide."\n";
        } else {
            echo '<p>'.$message_formulaire_invalide_spam."\n";
        }
    }
}

function editArticle($datas, $filename, $_CONFIGS=array()) {
    $ID=getIDbyFilename($datas, $filename);
    if (isset($_POST['title']) && isset($_POST['author']) && isset($_POST['date']) && isset($_POST['content']) && isset($_POST['chapo']) && isset($_POST['metadesc']) && isset($_POST['keywords']) && isset($_POST['cat']) && isset($_POST['sidebar']) && isset($_POST['img'])) {
        if ($ID>=0) $cdatas=$ID;
        else $cdatas=count($datas);
        $datas[$cdatas]['title']=$_POST['title'];
        $datas[$cdatas]['filename']=$filename;
        $datas[$cdatas]['author']=$_POST['author'];
        $datas[$cdatas]['date']=$_POST['date'];
        $datas[$cdatas]['chapo']=$_POST['chapo'];
        $datas[$cdatas]['cat']=$_POST['cat'];
        $datas[$cdatas]['metadesc']=$_POST['metadesc'];
        $datas[$cdatas]['keywords']=$_POST['keywords'];
        $datas[$cdatas]['sidebar']=$_POST['sidebar'];
        $datas[$cdatas]['position']=$_POST['position'];
        $datas[$cdatas]['img']=$_POST['img'];
        $position=array();
        $title=array();
        foreach ($datas as $key => $row) {
            $position[$key]  = $row['position'];
            $title[$key]  = $row['title'];
        }
        array_multisort($position, SORT_ASC,$title, SORT_ASC, $datas);
        $datas_=array();
        $a=0;
        foreach ($datas as $item) {
            $datas_[$a]=$item;
            $datas_[$a]['position']=$a;
            $a++;
        }
        $fp = fopen('datas/articles.json', 'w+');
        fwrite($fp, json_encode($datas_,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
        $fp = fopen('datas/articles/'.$filename, 'w+');
        fwrite($fp, str_replace("\\", "",$_POST['content']));
        fclose($fp);
    } else {
        $ID=getIDbyFilename($datas, $filename);
        //display form
        echo "<form method=\"POST\"><p>Titre : <input name=\"title\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['title']:"")."\"></p>",
        "<p>Image de pr&eacute;sentation : <input name=\"img\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['img']:"")."\"></p>",
        "<p>Auteur : <input name=\"author\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['author']:$_CONFIGS[0]['author'])."\"></p>",
        "<p>Date : <input name=\"date\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['date']:date('Y-m-d H:i:s'))."\"></p>",
        "<p>Chap&ocirc; : <textarea name=\"chapo\" id=\"chapo\" type=\"text\" style=\"width:100%;height:100px\">".($ID>=0?str_replace("\\", "",$datas[$ID]['chapo']):"")."</textarea></p>",
        "<p>Contenu de l&apos;article : <textarea id=\"content\" name=\"content\" style=\"width:100%;height:200px\">",
        (is_file('datas/articles/'.$filename)?htmlspecialchars(file_get_contents('datas/articles/'.$filename)):''),
        "</textarea></p>",
        "<p>Pr&eacute;visualisation :",
        "<div id=\"previewContent\" style=\"max-height:200px;overflow:auto;border:3px solid #333;\"></div></p><br>",
        "<script type=\"text/javascript\">
        update();
        var old=\"\";
        function update()
        {
          var chapo = document.getElementById('chapo');
          var textarea = document.getElementById('content');
          var preview = document.getElementById('previewContent');
          var allhtml=chapo.value+textarea.value;
          if (old != allhtml) {
            old = allhtml;
            preview.innerHTML=old;
          }
          window.setTimeout(update, 150);
        }
        </script>",
        "<p>Catégories : <input name=\"cat\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['cat']:"")."\"></p>",
        "<p>Meta description : <input name=\"metadesc\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['metadesc']:"")."\"></p>",
        "<p>Mots cl&eacute;s : <input name=\"keywords\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['keywords']:"")."\"></p>",
        "<p>Position SideBar : <select name=\"sidebar\"><option value='none' ".($ID>=0 && ($datas[$ID]['sidebar']!='left' || $datas[$ID]['sidebar']!='right') ?"selected":"").">Aucune</option><option value='right' ".($ID>=0 && $datas[$ID]['sidebar']=='right' ?"selected ":"").">Droite</option><option value='left' ".($ID>=0 && $datas[$ID]['sidebar']=='left' ?"selected ":"").">Gauche</option></select></p>",
        "<p>Position : <input name=\"position\" type=\"number\" value=\"".($ID>=0?(isset($datas[$ID]['position'])?$datas[$ID]['position']:count($datas)):count($datas))."\"></p>",
        "<p><button onclick=\"window.location.href='admin.php?module=article';return false;\">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;<button>Sauvegarder</button></p>",
        "</form>";
    }
}

function deleteArticle($datas, $filename) {
    $ID=getIDbyFilename($datas, $filename);
    unlink('datas/articles/'.$filename);
    unset($datas[$ID]);
    $datas2=array();
    foreach ($datas as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/articles.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

?>
