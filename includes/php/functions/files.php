<?php
//FILES
function listFiles($datas) {
    echo "<p><a style=\"margin-left:25px\" href=\"?module=file&edit=new\">Ajouter un fichier</a></p>";
    echo "<ul style=\"list-style-type:none;width:92%\">";
    $back=false;
    $i=0;
    foreach ($datas as $item) {
        if (preg_match('/\.(png|jpe?g|gif|svg)$/i',$item['filename'])) {
            $dimg="<img src=\"datas/files/".$item['filename']."\" width=\"auto\" height=\"50px\">";
        } else if (preg_match('/\.(mp3|oga|wav)$/i',$item['filename'])) {
            $dimg="<audio style=\"margin-top:15px\" controls>
              <source src=\"datas/files/".$item['filename']."\" type=\"audio/ogg\">
              <source src=\"datas/files/".$item['filename']."\" type=\"audio/mpeg\">
              <source src=\"datas/files/".$item['filename']."\" type=\"audio/wav\">
            Your browser does not support the audio element.
            </audio>";
        } else if (preg_match('/\.(mp4|ogg|ogv|webm)$/i',$item['filename'])) {
            $dimg="<video style=\"margin-top:15px\" controls>
              <source src=\"datas/files/".$item['filename']."\" type=\"video/ogg\">
              <source src=\"datas/files/".$item['filename']."\" type=\"video/mp4\">
              <source src=\"datas/files/".$item['filename']."\" type=\"video/webm\">
            Your browser does not support the video element.
            </video>";
        } else $dimg="";
        echo "<li style=\"vertical-align:middle;min-height:50px;padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
        echo "<a href=\"?module=file&edit=",$item['filename'],"\" style=\"color:",($back?"#333;":"#eee;"),"\">",$dimg,"<div style=\"margin-top:18px;margin-left:10px;display:inline;position:absolute;\">",$item['title'],"</div></a><a href=\"?module=file&del=",$item['filename'],"\" style=\"margin-top:18px;margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir supprimer le fichier ".$item['title']." ?') ) { return true; } else { return false; }\">Supprimer</a><a href=\"datas/files/".$item['filename']."\" target=\"_BLANK\" style=\"margin-right:25px;margin-top:18px;float:right;color:",($back?"#333;":"#eee;"),"\">Voir</a></li>";
        if ($back) $back=false;
        else $back=true;
        $i++;
    }
    echo "</ul>",
    "<p><a style=\"margin-left:25px\" href=\"?module=file&edit=new\">Ajouter un fichier</a></p>";
}

function displayFile($datas, $id) {
    //FIXME
}

function getIDFileByFilename($datas, $filename) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['filename']==$filename) {
            return $i;
        }
        $i++;
    }
    return -1;
}
function getIDFileByTitle($datas, $title) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['title']==$title) {
            return $i;
        }
        $i++;
    }
    return -1;
}


function editFile($datas, $filename, $_CONFIGS=array()) {
    
    if (isset($_POST['title']) && isset($_POST['author']) && isset($_POST['date']) && isset($_POST['position']) && isset($_FILES['filename'])) {
        if (isset($_POST['oldfilename'])) $ID=getIDFileByFilename($datas, $_POST['oldfilename']);
        else $ID=getIDFileByFilename($datas, basename($_FILES['filename']['name']));
        if (isset($_FILES['filename']['tmp_name'])) {
            $uploaddir = realpath('./').'/datas/files/';
            $uploadfile = $uploaddir . basename($_FILES['filename']['name']);
            move_uploaded_file($_FILES['filename']['tmp_name'], $uploadfile);
        }
        if (isset($_POST['oldfilename']) && !empty($_FILES['filename']['tmp_name'])) unlink('datas/files/'.$_POST['oldfilename']);
        if ($ID>=0) $cdatas=$ID;
        else $cdatas=count($datas);
        $datas[$cdatas]['title']=$_POST['title'];
        $datas[$cdatas]['filename']=(!empty($_FILES['filename']['tmp_name'])?basename($_FILES['filename']['name']):$_POST['oldfilename']);
        $datas[$cdatas]['author']=$_POST['author'];
        $datas[$cdatas]['date']=$_POST['date'];
        $datas[$cdatas]['position']=$_POST['position'];
        $position=array();
        $title=array();
        foreach ($datas as $key => $row) {
            $position[$key]  = $row['position'];
            $title[$key]  = $row['title'];
        }
        array_multisort($position, SORT_ASC,$title, SORT_ASC, $datas);
        $datas_=array();
        $a=0;
        foreach ($datas as $item) {
            $datas_[$a]=$item;
            $datas_[$a]['position']=$a;
            $a++;
        }
        $fp = fopen('datas/files.json', 'w+');
        fwrite($fp, json_encode($datas_,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
    } else {
        if ($filename=='new') $ID=-1;
        else $ID=getIDFileByFilename($datas, $filename);
        //display form
        echo "<form method=\"POST\" enctype=\"multipart/form-data\"><p>Titre : <input name=\"title\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['title']:"")."\"></p>",
        "<p>Auteur : <input name=\"author\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['author']:$_CONFIGS[0]['author'])."\"></p>",
        "<p>Envoyez ce fichier : <input name=\"filename\" type=\"file\" /></p>",
        "<p>Date : <input name=\"date\" type=\"date\" value=\"".($ID>=0?$datas[$ID]['date']:date('Y-m-d'))."\"></p>",
        ($ID>=0?"<input name=\"oldfilename\" type=\"hidden\" value=\"".$filename."\">":""),
        "<p>Position : <input name=\"position\" type=\"number\" value=\"".($ID>=0?(isset($datas[$ID]['position'])?$datas[$ID]['position']:count($datas)):count($datas))."\"></p>",
        "<p><button onclick=\"window.location.href='admin.php?module=file';return false;\">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;<button>Sauvegarder</button></p>",
        "</form>";
    }
}

function deleteFile($datas, $filename) {
    $ID=getIDFileByFilename($datas, $filename);
    if (file_exists('datas/files/'.$filename)) unlink('datas/files/'.$filename);
    unset($datas[$ID]);
    $datas2=array();
    foreach ($datas as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/files.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

?>
