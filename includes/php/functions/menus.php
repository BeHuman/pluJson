<?php
//MENUS
function listMenus($datas) {
    echo "<p><a style=\"margin-left:25px\" href=\"?module=menu&edit=new&timestamp=",hash('sha256', date('Y-m-d H:i:s')),"\">Ajouter un menu</a></p>";
    echo "<ul style=\"list-style-type:none;width:92%\">";
    $back=false;
    foreach ($datas as $item) {
        echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
        echo "<a href=\"?module=menu&edit=",$item['link'],"&timestamp=",(isset($item['timestamp'])?$item['timestamp']:hash('sha256', date('Y-m-d H:i:s'))),"\" style=\"color:",($back?"#333;":"#eee;"),"\">",$item['title'],"</a> <a href=\"?module=menu&del=",urlencode((isset($item['timestamp'])?$item['timestamp']:hash('sha256', date('Y-m-d H:i:s')))),"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir suprimmer le menu ".$item['title']." ?') ) { return true; } else { return false; }\">Supprimer</a></li>";
        if ($back) $back=false;
        else $back=true;
    }
    echo "</ul>",
    "<p><a style=\"margin-left:25px\" href=\"?module=menu&edit=new&timestamp=".hash('sha256', date('Y-m-d H:i:s'))."\">Ajouter un menu</a></p>";
}

function displayMenu($datas, $id) {
    //FIXME
}

function getIDMenuByTimestamp($datas, $timestamp) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['timestamp']==$timestamp) {
            return $i;
        }
        $i++;
    }
    return -1;
}

function editMenu($datas, $link, $_CONFIGS=array(), $timestamp=0) {
    
    if (isset($_POST['title']) && isset($_POST['link']) && isset($_POST['target']) && isset($_POST['styleli']) && isset($_POST['stylelink']) && isset($_POST['timestamp'])) {
        $ID=getIDMenuByTimestamp($datas, (isset($_POST['timestamp'])?$_POST['timestamp']:$timestamp));
        if ($ID>=0) $cdatas=$ID;
        else $cdatas=count($datas);
        $datas[$cdatas]['title']=$_POST['title'];
        $datas[$cdatas]['link']=$_POST['link'];
        $datas[$cdatas]['target']=$_POST['target'];
        $datas[$cdatas]['styleli']=$_POST['styleli'];
        $datas[$cdatas]['stylelink']=$_POST['stylelink'];
        $datas[$cdatas]['timestamp']=$_POST['timestamp'];
        $datas[$cdatas]['position']=$_POST['position'];
        $position=array();
        $title=array();
        foreach ($datas as $key => $row) {
            $position[$key]  = $row['position'];
            $title[$key]  = $row['title'];
        }
        array_multisort($position, SORT_ASC,$title, SORT_ASC, $datas);
        $datas_=array();
        $a=0;
        foreach ($datas as $item) {
            $datas_[$a]=$item;
            $datas_[$a]['position']=$a;
            $a++;
        }
        $fp = fopen('datas/menus.json', 'w+');
        fwrite($fp, json_encode($datas_,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
    } else {
        $ID=getIDMenuByTimestamp($datas, (isset($_GET['timestamp'])?$_GET['timestamp']:$timestamp));
        //display form
        echo "<form method=\"POST\"><p>Titre : <input name=\"title\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['title']:"")."\"></p>",
            "<p>Lien : <input name=\"link\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['link']:"")."\"></p>",
            "<p>Target : <input name=\"target\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['target']:"")."\"></p>",
            "<p>Position : <input name=\"position\" type=\"number\" value=\"".($ID>=0?$datas[$ID]['position']:count($datas))."\"></p>",
            "<p>style LI : <input name=\"styleli\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['styleli']:"")."\"></p>",
            "<p>style LINK : <input name=\"stylelink\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['stylelink']:"")."\"></p>",
            "<input name=\"timestamp\" type=\"hidden\" value=\"".($ID>=0?$datas[$ID]['timestamp']:$timestamp)."\">",
            "<p><button onclick=\"window.location.href='admin.php?module=menu';return false;\">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;<button>Sauvegarder</button></p>",
            "</form>";
    }
}

function deleteMenu($datas, $timestamp) {
    $ID=getIDMenuByTimestamp($datas, $timestamp);
    unset($datas[$ID]);
    $datas2=array();
    foreach ($datas as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/menus.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

?>
