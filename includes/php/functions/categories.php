<?php
//CATEGORIES
function listCategories($datas) {
    echo "<p><a style=\"margin-left:25px\" href=\"?module=categorie&edit=",hash('sha256', date('Y-m-d H:i:s')),".php\">Ajouter une categorie</a></p>";
    echo "<ul style=\"list-style-type:none;width:92%\">";
    $back=false;
    $i=0;
    foreach ($datas as $item) {
        echo "<li style=\"padding:5px;background-color:",($back?"#eee;":"#333;"),"color:",($back?"#333;":"#eee;"),"\">";
        echo "<a href=\"?module=categorie&edit=",$item['title'],"\" style=\"color:",($back?"#333;":"#eee;"),"\">",$item['title'],"</a> <a href=\"?module=categorie&del=",urlencode($item['title']),"\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\" onclick=\"if( confirm('Etes vous sure de vouloir supprimer la categorie ".$item['title']." ?') ) { return true; } else { return false; }\">Supprimer</a><a href=\"index.php?categorie=".$i."\" target=\"_BLANK\" style=\"margin-right:25px;float:right;color:",($back?"#333;":"#eee;"),"\">Voir</a></li>";
        if ($back) $back=false;
        else $back=true;
        $i++;
    }
    echo "</ul>",
    "<p><a style=\"margin-left:25px\" href=\"?module=categorie&edit=new\">Ajouter une categorie</a></p>";
}

function getIDCategorie($datas, $title) {
    $i=0;
    foreach ($datas as $item) {
        if ($item['title']==urldecode($title)) {
            return $i;
        }
        $i++;
    }
    return -1;
}

function editCategorie($datas, $link, $_CONFIGS=array()) {
    
    if (isset($_POST['title'])) {
        $ID=getIDCategorie($datas, $_POST['title']);
        if ($ID>=0) $cdatas=$ID;
        else $cdatas=count($datas);
        $datas[$cdatas]['title']=$_POST['title'];
        $datas[$cdatas]['position']=$_POST['position'];
        $position=array();
        $title=array();
        foreach ($datas as $key => $row) {
            $position[$key]  = $row['position'];
            $title[$key]  = $row['title'];
        }
        array_multisort($position, SORT_ASC,$title, SORT_ASC, $datas);
        $datas_=array();
        $a=0;
        foreach ($datas as $item) {
            $datas_[$a]=$item;
            $datas_[$a]['position']=$a;
            $a++;
        }
        $fp = fopen('datas/categories.json', 'w+');
        fwrite($fp, json_encode($datas_,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
        fclose($fp);
    } else {
        $ID=getIDCategorie($datas, (isset($_POST['title'])?$_POST['title']:$link));
        //display form
        echo "<form method=\"POST\"><p>Titre : <input name=\"title\" type=\"text\" value=\"".($ID>=0?$datas[$ID]['title']:"")."\"></p>",
            "<p>Position : <input name=\"position\" type=\"number\" value=\"".($ID>=0?(isset($datas[$ID]['position'])?$datas[$ID]['position']:count($datas)):count($datas))."\"></p>",
            "<p><button onclick=\"window.location.href='admin.php?module=categorie';return false;\">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;<button>Sauvegarder</button></p>",
            "</form>";
    }
}

function deleteCategorie($datas, $title) {
    $ID=getIDCategorie($datas, $title);
    unset($datas[$ID]);
    $datas2=array();
    foreach ($datas as $item) {
        $datas2[]=$item;
    }
    $fp = fopen('datas/categories.json', 'w+');
    fwrite($fp, json_encode($datas2,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    fclose($fp);
}

?>
