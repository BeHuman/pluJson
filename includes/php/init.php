<?php
session_start();
error_reporting(E_ALL);
header('Content-Type: text/html; charset=utf-8');

//==========================FUNCTIONS GLOBAL==============================
/*
* cette fonction sert à nettoyer et enregistrer un texte
*/
function Rec($text) {
    $text = htmlspecialchars(trim($text), ENT_QUOTES);
    if (1 === get_magic_quotes_gpc()) {
        $text = stripslashes($text);
    }
    $text = nl2br($text);
    return $text;
}
/*
* Cette fonction sert à vérifier la syntaxe d'un email
*/
function IsEmail($email) {
    $value = preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $email);
    return (($value === 0) || ($value === false)) ? false : true;
}
/*
* function de connection au BO
*/
function connected($_ADMINS) {
    if ( (isset($_SESSION['user']) && isset($_SESSION['pwd']))){
        foreach ($_ADMINS as $item) {
            if ($item['user']==$_SESSION['user'] && $item['pwd']==$_SESSION['pwd']) {
                return true;
            }
        }
        return false;
    } else {
        return false;
    }
}
//==========================FUNCTIONS STOP==============================

if (!isset($_GET['lang'])) {
    $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    //$lang = $language{0}.$language{1};
    $lang = (file_exists('includes/langs/'.$lang.'.json')) ? $lang : 'fr';
} else {
    $lang =='';
    $lang = (isset($_GET['lang']) && file_exists('includes/langs/'.$_GET['lang'].'.json')) ? $_GET['lang'] : 'fr';
}

$_LANGS = json_decode(file_get_contents('includes/langs/'.$lang.'.json'), true);
$_CONFIGS = json_decode(file_get_contents('datas/configs.json'), true);
$_ARTICLES = json_decode(file_get_contents('datas/articles.json'), true);
$_PAGES = json_decode(file_get_contents('datas/pages.json'), true);
$__FILES = json_decode(file_get_contents('datas/files.json'), true);
$_CATEGORIES = json_decode(file_get_contents('datas/categories.json'), true);
$_MENUS = json_decode(file_get_contents('datas/menus.json'), true);
$_ADMINS = json_decode(file_get_contents('datas/admins.json'), true);

?>
