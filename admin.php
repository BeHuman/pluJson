<?php
include('includes/php/init.php');
include('includes/php/functions/configs.php');
include('includes/php/functions/admins.php');
include('includes/php/functions/articles.php');
include('includes/php/functions/menus.php');
include('includes/php/functions/categories.php');
include('includes/php/functions/pages.php');
include('includes/php/functions/files.php');
?>
<!DOCTYPE html>
<html>
    <head>
    <?php include('includes/tpl_admin/head.php'); ?>
    </head>
    <body>
        <?php include('includes/tpl_admin/header.php'); ?>
        <?php include('includes/tpl_admin/content.php'); ?>
        <?php include('includes/tpl_admin/footer.php'); ?>
    </body>
</html>
